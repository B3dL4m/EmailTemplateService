package com.ansf.EmailTemplateService.rest;

import com.ansf.EmailTemplateService.model.User;
import com.ansf.EmailTemplateService.model.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserRegistrationController.class)
@TestPropertySource("classpath:application-test.properties")
public class UserRegistrationControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private UserRepository repository;

    @Test
    public void listUsersEmpty() throws Exception {
        given(repository.findAll()).willReturn(new ArrayList<>());
        mockMvc.perform(get("/user"))
                .andExpect(status().isOk())
                .andExpect(content().string(mapper.writeValueAsString(new ArrayList<User>())));
    }

    @Test
    public void listUsers() throws Exception {
        List<User> users = new ArrayList<>();
        User user1 = new User("john.doe@gmail.com", "DEADBEEF");
        user1.setId(1L);
        users.add(user1);

        given(repository.findAll()).willReturn(users);
        mockMvc.perform(get("/user"))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(users)));
    }

    @Test
    public void getExistingUser() throws Exception {
        List<User> users = new ArrayList<>();
        Long id = 1L;
        User user1 = new User("john.doe@gmail.com", "DEADBEEF");
        user1.setId(id);
        users.add(user1);

        given(repository.findById(id)).willReturn(Optional.of(user1));
        mockMvc.perform(get("/user/" + id))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(user1)));
    }

    @Test
    public void getUnexistingUser() throws Exception {
        Long id = 1L;

        given(repository.findById(id)).willReturn(Optional.empty());
        mockMvc.perform(get("/user/" + id))
                .andExpect(status().isNotFound())
                .andExpect(content().string("User with id " + id + " not found."));
    }

    @Test
    public void createUser() throws Exception {
        String email = "john.doe@gmail.com";
        User user = new User(email, "DEADBEEF");

        given(repository.findByEmail(email)).willReturn(null);
        mockMvc.perform(post("/user")
                    .content(mapper.writeValueAsString(user))
                    .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(user)));
    }

    @Test
    public void createUserEmailAlreadyExists() throws Exception {
        String email = "john.doe@gmail.com";
        User user = new User(email, "DEADBEEF");

        given(repository.findByEmail(email)).willReturn(user);
        mockMvc.perform(post("/user")
                    .content(mapper.writeValueAsString(user))
                    .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andExpect(content().string("Unable to create new user. "
                        + "A User with email " + email + " already exists."));
    }

    /** Todo
     *  Exception handler cannot read message from provider.
     *  Is it because MockMvc doesn't run in a servlet container?
     *  Should i use a WebMvcContext?
     */
//    @Test
//    public void createUserInvalidEmail() throws Exception {
//        String email = "johnDoe";
//        User user = new User(email, "DEADBEEF");
//
//        mockMvc.perform(post("/user")
//                    .content(mapper.writeValueAsString(user))
//                    .contentType(MediaType.APPLICATION_JSON))
//                .andDo(print())
//                .andExpect(status().isBadRequest());
//    }
//
//    @Test
//    public void createUserEmptyEmail() throws Exception {
//        User user = new User(null, "DEADBEEF");
//
//        mockMvc.perform(post("/user")
//                .content(mapper.writeValueAsString(user))
//                .contentType(MediaType.APPLICATION_JSON))
//                .andDo(print())
//                .andExpect(status().isBadRequest());
//    }
//
//    @Test
//    public void createUserEmptyPassword() throws Exception {
//        String email = "john.doe@gmail.com";
//        User user = new User(email, null);
//
//        mockMvc.perform(post("/user")
//                    .content(mapper.writeValueAsString(user))
//                    .contentType(MediaType.APPLICATION_JSON))
//                .andDo(print())
//                .andExpect(status().isBadRequest());
//    }

    @Test
    public void updateExistingUser() throws Exception {
        User oldUser = new User("john.doe@gmail.com", "DEADBEEF");
        User newUser = new User("john.carmack@gmail.com", "D00M");
        Long id = 1L;

        given(repository.findById(id)).willReturn(Optional.of(oldUser));
        mockMvc.perform(put("/user/" + id)
                    .content(mapper.writeValueAsString(newUser))
                    .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(newUser)));
    }

    @Test
    public void updateUnexistingUser() throws Exception {
        User newUser = new User("john.carmack@gmail.com", "D00M");
        Long id = 1L;

        given(repository.findById(id)).willReturn(Optional.empty());
        mockMvc.perform(put("/user/" + id)
                    .content(mapper.writeValueAsString(newUser))
                    .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string("User with id " + id + " not found."));
    }

    @Test
    public void deleteExistingUser() throws Exception {
        User user = new User("john.carmack@gmail.com", "D00M");
        Long id = 1L;

        given(repository.findById(id)).willReturn(Optional.of(user));
        mockMvc.perform(delete("/user/" + id))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }

    @Test
    public void deleteUnexistingUser() throws Exception {
        User user = new User("john.carmack@gmail.com", "D00M");
        Long id = 1L;

        given(repository.findById(id)).willReturn(Optional.empty());
        mockMvc.perform(delete("/user/" + id))
                .andExpect(status().isNotFound())
                .andExpect(content().string("User with id " + id + " not found."));
    }
}
