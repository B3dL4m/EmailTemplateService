package com.ansf.EmailTemplateService;

import com.ansf.EmailTemplateService.rest.EmailController;
import com.ansf.EmailTemplateService.rest.TemplateController;
import com.ansf.EmailTemplateService.rest.UserRegistrationController;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@TestPropertySource("classpath:application-test.properties")
public class EmailTemplateServiceApplicationTests {

    @Autowired
    private UserRegistrationController userRegistrationController;

    @Autowired
    private TemplateController templateController;

    @Autowired
    private EmailController emailController;

	@Test
	public void contextLoads() {
        Assertions.assertThat(userRegistrationController).isNotNull();
        Assertions.assertThat(templateController).isNotNull();
        Assertions.assertThat(emailController).isNotNull();
    }
}
