package com.ansf.EmailTemplateService.service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.transfer.exception.FileLockException;
import com.amazonaws.util.IOUtils;
import com.ansf.EmailTemplateService.TestConfiguration;
import com.ansf.EmailTemplateService.exception.InexistentTemplateException;
import com.ansf.EmailTemplateService.exception.InexistentUserException;
import com.ansf.EmailTemplateService.exception.TemplateExistsException;
import com.ansf.EmailTemplateService.model.StorageReference;
import com.ansf.EmailTemplateService.model.Template;
import com.ansf.EmailTemplateService.model.User;
import com.ansf.EmailTemplateService.model.repository.StorageReferenceRepository;
import com.ansf.EmailTemplateService.model.repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {TestConfiguration.class},
        webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class AWSTemplateStorageServiceTests {

    private static Logger logger;
    private static Map<String, File> testTemplates;

    @Autowired
    private String s3Bucket;

    @Autowired
    private String s3BucketKey;

    @Autowired
    private Long s3RetryCount;

    @Autowired
    private String s3Region;

    @Autowired
    private AWSTemplateStorageService service;

    @Autowired
    private String checksumAlgorithm;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private StorageReferenceRepository storageReferenceRepository;

    @MockBean
    private ChecksumCalculator checksumCalculator;

    @MockBean
    private AmazonS3 amazonS3;

    @BeforeClass
    public static void initTests() {
        logger = LoggerFactory.getLogger(AWSTemplateStorageServiceTests.class);

        ClassLoader classLoader = AWSTemplateStorageServiceTests.class.getClassLoader();
        testTemplates = new HashMap<>();
        testTemplates.put("template1", new File(classLoader.getResource("template1.html").getPath()));
        testTemplates.put("template2", new File(classLoader.getResource("template2.html").getPath()));
    }

    @Test
    public void getExistingTemplate() {
        Template expected = new Template("template1", "DEADBEEF");
        Template actual;
        User user = new User("john.doe@gmail.com", "12345");
        user.addTemplate(expected);

        try (InputStream inputStream =  new FileInputStream(testTemplates.get("template1"))) {
            S3Object s3Object = new S3Object();
            s3Object.setObjectContent(inputStream);
            expected.setContents(IOUtils.toByteArray(inputStream));

            given(userRepository.findById(1L)).willReturn(
                    Optional.of(user));
            given(amazonS3.getObject(s3Bucket, expected.getStorageId())).willReturn(s3Object);

            actual = service.getTemplate(1L, "template1");

            Assertions.assertThat(expected).isEqualTo(actual);
        } catch (Exception e) {
            logger.error(e.getMessage());
            // force test to fail
            Assert.assertFalse(true);
        }
    }

    @Test
    public void getExistingTemplateRetry() {
        Template expected = new Template("template1", "DEADBEEF");
        Template actual;
        User user = new User("john.doe@gmail.com", "12345");
        user.addTemplate(expected);

        try (InputStream inputStream =  new FileInputStream(testTemplates.get("template1"))) {
            S3Object s3Object = new S3Object();
            s3Object.setObjectContent(inputStream);
            expected.setContents(IOUtils.toByteArray(inputStream));

            given(userRepository.findById(1L)).willReturn(
                    Optional.of(user));
            when(amazonS3.getObject(s3Bucket, expected.getStorageId()))
                    .thenThrow(AmazonClientException.class)
                    .thenReturn(s3Object);

            actual = service.getTemplate(1L, "template1");

            Assertions.assertThat(expected).isEqualTo(actual);
        } catch (Exception e) {
            logger.error(e.getMessage());
            // force test to fail
            Assert.assertFalse(true);
        }
    }

    @Test
    public void getInexistentTemplate() {
        User user = new User("john.doe@gmail.com", "12345");
        Template expected = new Template("template1", "DEADBEEF");

        given(userRepository.findById(1L)).willReturn(
                Optional.of(user));

        try {
            service.getTemplate(1L, "template1");
            Assert.assertFalse(true);
        } catch (InexistentTemplateException e) {
            Assert.assertTrue(true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void getTemplateInexistentUser() {
        Template expected = new Template("template1", "DEADBEEF");

        given(userRepository.findById(1L)).willReturn(
                Optional.empty());

        try {
            service.getTemplate(1L, "template1");
            Assert.assertFalse(true);
        } catch (InexistentUserException e) {
            Assert.assertTrue(true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void getTemplateRetryCountExceeded() {
        Template expected = new Template("template1", "DEADBEEF");
        Template actual;
        User user = new User("john.doe@gmail.com", "12345");
        user.addTemplate(expected);

        given(userRepository.findById(1L)).willReturn(
                Optional.of(user));
        given(amazonS3.getObject(s3Bucket, expected.getStorageId())).willThrow(AmazonClientException.class);

        try {
            service.getTemplate(1L, "template1");
            Assert.assertFalse(true);
        } catch (AmazonClientException e) {
            Assert.assertTrue(true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void getTemplateExceptionNotRetryable() {
        Template expected = new Template("template1", "DEADBEEF");
        Template actual;
        User user = new User("john.doe@gmail.com", "12345");
        user.addTemplate(expected);

        given(userRepository.findById(1L)).willReturn(
                Optional.of(user));
        given(amazonS3.getObject(s3Bucket, expected.getStorageId())).willThrow(FileLockException.class);

        try {
            service.getTemplate(1L, "template1");
            Assert.assertFalse(true);
        } catch (FileLockException e) {
            Assert.assertTrue(true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void getTemplates() {
        User user = new User("john.doe@gmail.com", "12345");
        Template template1 = new Template("template1", "DEADBEEF");
        Template template2 = new Template("template2", "1337H4XX");
        List<Template> expected = new ArrayList<>();
        expected.add(template1);
        expected.add(template2);
        user.addTemplates(expected);

        given(userRepository.findById(1L)).willReturn(
                Optional.of(user));

        try {
            List<Template> actual = service.getTemplates(1L);
            for (int i = 0; i < expected.size(); ++i) {
                Assertions.assertThat(expected.get(i)).isEqualTo(actual.get(i));
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void getTemplatesInexistentUser() {
        given(userRepository.findById(1L)).willReturn(
                Optional.empty());

        try {
            service.getTemplates(1L);
            // shouldn't get here
            Assert.assertFalse(true);
        } catch (InexistentUserException e) {
            Assert.assertTrue(true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void addTemplate() {
        User user = new User(1L, "john.doe@gmail.com", "12345");
        String digestedFileName = "DEADBEEF";
        String s3key = s3BucketKey + '/' + digestedFileName;
        Template template1 = new Template("template1", s3key, user);
        byte[] contents = "template1".getBytes();

        try {
            given(userRepository.findById(user.getId())).willReturn(Optional.of(user));
            given(checksumCalculator.calculate(contents)).willReturn(digestedFileName);
            given(amazonS3.doesBucketExistV2(s3Bucket)).willReturn(true);
            given(amazonS3.doesObjectExist(s3Bucket, s3key)).willReturn(false);
            given(storageReferenceRepository.findByStorageId(s3key)).willReturn(null);

            service.addTemplate(user.getId(), template1.getName(), contents);

            Assertions.assertThat(user.getTemplates().get(0).getName()).isEqualTo(template1.getName());
            Assertions.assertThat(user.getTemplates().get(0).getStorageId()).isEqualTo(template1.getStorageId());
            Assertions.assertThat(user.getTemplates().get(0).getUser()).isEqualTo(template1.getUser());
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void addTemplateInexistentUser() {
        User user = new User(1L, "john.doe@gmail.com", "12345");
        String templateName = "template";
        byte[] contents = templateName.getBytes();

        try {
            given(userRepository.findById(user.getId())).willReturn(Optional.empty());

            service.addTemplate(user.getId(), templateName, contents);
            Assert.assertFalse(true);
        } catch (InexistentUserException e) {
            Assert.assertTrue(true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void addTemplateWrongChecksumAlgorithm() {
        User user = new User(1L, "john.doe@gmail.com", "12345");
        String templateName = "template";
        byte[] contents = "templateName".getBytes();

        try {
            given(userRepository.findById(user.getId())).willReturn(Optional.of(user));
            given(checksumCalculator.calculate(contents)).willThrow(NoSuchAlgorithmException.class);

            service.addTemplate(user.getId(), templateName, contents);
            Assert.assertFalse(true);
        } catch (NoSuchAlgorithmException e) {
            Assert.assertTrue(true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void addTemplateBucketDoesNotExistCanCreate() {
        User user = new User(1L, "john.doe@gmail.com", "12345");
        String digestedFileName = "DEADBEEF";
        String s3key = s3BucketKey + '/' + digestedFileName;
        Template template1 = new Template("template1", s3key, user);
        byte[] contents = "template1".getBytes();

        try {
            given(userRepository.findById(user.getId())).willReturn(Optional.of(user));
            given(checksumCalculator.calculate(contents)).willReturn(digestedFileName);
            given(amazonS3.doesBucketExistV2(s3Bucket)).willReturn(false);
            given(amazonS3.doesObjectExist(s3Bucket, s3key)).willReturn(false);
            given(storageReferenceRepository.findByStorageId(s3key)).willReturn(null);

            service.addTemplate(user.getId(), template1.getName(), contents);

            Assertions.assertThat(user.getTemplates().get(0).getName()).isEqualTo(template1.getName());
            Assertions.assertThat(user.getTemplates().get(0).getStorageId()).isEqualTo(template1.getStorageId());
            Assertions.assertThat(user.getTemplates().get(0).getUser()).isEqualTo(template1.getUser());
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void addTemplateAmazonException() {
        User user = new User(1L, "john.doe@gmail.com", "12345");
        String digestedFileName = "DEADBEEF";
        String templateName = "template";
        byte[] contents = templateName.getBytes();
        CreateBucketRequest request = new CreateBucketRequest(s3Bucket, s3Region);

        try {
            given(userRepository.findById(user.getId())).willReturn(Optional.of(user));
            given(checksumCalculator.calculate(contents)).willReturn(digestedFileName);
            given(amazonS3.doesBucketExistV2(s3Bucket)).willThrow(AmazonClientException.class);

            service.addTemplate(user.getId(), templateName, contents);
            Assert.assertFalse(true);
        } catch (AmazonClientException e) {
            Assert.assertTrue(true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void addTemplateReferenceInconsistency() {
        User user = new User(1L, "john.doe@gmail.com", "12345");
        String digestedFileName = "DEADBEEF";
        String s3key = s3BucketKey + '/' + digestedFileName;
        Template template1 = new Template("template1", s3key, user);
        byte[] contents = "template1".getBytes();

        try {
            given(userRepository.findById(user.getId())).willReturn(Optional.of(user));
            given(checksumCalculator.calculate(contents)).willReturn(digestedFileName);
            given(amazonS3.doesBucketExistV2(s3Bucket)).willReturn(true);
            given(amazonS3.doesObjectExist(s3Bucket, s3key)).willReturn(true);
            given(storageReferenceRepository.findByStorageId(s3key)).willReturn(null);

            service.addTemplate(user.getId(), template1.getName(), contents);
            Assert.assertFalse(true);
        } catch (RuntimeException e) {
            Assert.assertTrue(true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void addTemplateObjectContentExists() {
        User user = new User(1L, "john.doe@gmail.com", "12345");
        String digestedFileName = "DEADBEEF";
        String s3key = s3BucketKey + '/' + digestedFileName;
        Template template1 = new Template("template1", s3key, user);
        byte[] contents = "template1".getBytes();
        StorageReference storageReference = new StorageReference(s3key, 1L);

        try {
            given(userRepository.findById(user.getId())).willReturn(Optional.of(user));
            given(checksumCalculator.calculate(contents)).willReturn(digestedFileName);
            given(amazonS3.doesBucketExistV2(s3Bucket)).willReturn(true);
            given(amazonS3.doesObjectExist(s3Bucket, s3key)).willReturn(true);
            given(storageReferenceRepository.findByStorageId(s3key)).willReturn(storageReference);

            service.addTemplate(user.getId(), template1.getName(), contents);

            Assertions.assertThat(user.getTemplates().get(0).getName()).isEqualTo(template1.getName());
            Assertions.assertThat(user.getTemplates().get(0).getStorageId()).isEqualTo(template1.getStorageId());
            Assertions.assertThat(user.getTemplates().get(0).getUser()).isEqualTo(template1.getUser());
            Assertions.assertThat(storageReference.getCount()).isEqualTo(2L);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void addTemplateAliasExists() {
        User user = new User(1L, "john.doe@gmail.com", "12345");
        String digestedFileName = "DEADBEEF";
        String s3key = s3BucketKey + '/' + digestedFileName;
        Template template1 = new Template("template1", s3key, user);
        byte[] contents = "template1".getBytes();

        user.addTemplate(template1);

        try {
            given(userRepository.findById(user.getId())).willReturn(Optional.of(user));
            given(checksumCalculator.calculate(contents)).willReturn(digestedFileName);
            given(amazonS3.doesBucketExistV2(s3Bucket)).willReturn(true);
            given(amazonS3.doesObjectExist(s3Bucket, s3key)).willReturn(false);

            service.addTemplate(user.getId(), template1.getName(), contents);
            Assert.assertFalse(true);
        } catch (TemplateExistsException e) {
            Assert.assertTrue(true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void removeExistingTemplateSingularReference() {
        User user = new User(1L, "john.doe@gmail.com", "12345");
        String digestedFileName = "DEADBEEF";
        String s3key = s3BucketKey + '/' + digestedFileName;
        Template template = new Template("expected", s3key, user);
        StorageReference storageReference = new StorageReference(s3key, 1L);

        user.addTemplate(template);

        given(userRepository.findById(user.getId())).willReturn(Optional.of(user));
        given(storageReferenceRepository.findByStorageId(s3key)).willReturn(storageReference);

        try {
            service.removeTemplate(user.getId(), template.getName());

            verify(storageReferenceRepository).delete(storageReference);
            verify(storageReferenceRepository).flush();
            Assertions.assertThat(user.getTemplates().size()).isEqualTo(0);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void removeExistingTemplateMultipleReference() {
        User user = new User(1L, "john.doe@gmail.com", "12345");
        String digestedFileName = "DEADBEEF";
        String s3key = s3BucketKey + '/' + digestedFileName;
        Template template = new Template("expected", s3key, user);
        StorageReference storageReference = new StorageReference(s3key, 2L);

        user.addTemplate(template);

        given(userRepository.findById(user.getId())).willReturn(Optional.of(user));
        given(storageReferenceRepository.findByStorageId(s3key)).willReturn(storageReference);

        try {
            service.removeTemplate(user.getId(), template.getName());

            verify(storageReferenceRepository).saveAndFlush(storageReference);
            Assertions.assertThat(storageReference.getCount()).isEqualTo(1L);
            Assertions.assertThat(user.getTemplates().size()).isEqualTo(0);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void removeTemplateInexistentUser() {
        User user = new User(1L, "john.doe@gmail.com", "12345");
        String digestedFileName = "DEADBEEF";
        String s3key = s3BucketKey + '/' + digestedFileName;
        Template template = new Template("expected", s3key, user);

        given(userRepository.findById(user.getId())).willReturn(Optional.empty());

        try {
            service.removeTemplate(user.getId(), template.getName());
        } catch (InexistentUserException e) {
            Assert.assertTrue(true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void removeInexistentTemplate() {
        User user = new User(1L, "john.doe@gmail.com", "12345");
        String digestedFileName = "DEADBEEF";
        String s3key = s3BucketKey + '/' + digestedFileName;
        Template template = new Template("expected", s3key, user);

        given(userRepository.findById(user.getId())).willReturn(Optional.of(user));

        try {
            service.removeTemplate(user.getId(), template.getName());
        } catch (InexistentTemplateException e) {
            Assert.assertTrue(true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void removeAllTemplates() {
        User user = new User(1L, "john.doe@gmail.com", "12345");
        Template template1 = new Template("template1", "H4XX0R");
        Template template2 = new Template("template2", "DEADBEEF");
        StorageReference storageReference1 = new StorageReference(template1.getStorageId(), 1L);
        StorageReference storageReference2 = new StorageReference(template1.getStorageId(), 2L);

        user.addTemplate(template1);
        user.addTemplate(template2);

        given(userRepository.findById(user.getId())).willReturn(Optional.of(user));
        given(storageReferenceRepository.findByStorageId(template1.getStorageId())).willReturn(storageReference1);
        given(storageReferenceRepository.findByStorageId(template2.getStorageId())).willReturn(storageReference2);

        try {
            service.removeAllTemplates(user.getId());

            verify(storageReferenceRepository).delete(storageReference1);
            verify(storageReferenceRepository).flush();
            verify(storageReferenceRepository).saveAndFlush(storageReference2);
            Assert.assertEquals(user.getTemplates().size(), 0);
            Assertions.assertThat(storageReference2.getCount()).isEqualTo(1L);
        } catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }

    @Test
    public void removeAllTemplatesInexistentUser() {
        User user = new User(1L, "john.doe@gmail.com", "12345");

        given(userRepository.findById(user.getId())).willReturn(Optional.empty());

        try {
            service.removeAllTemplates(user.getId());
            Assert.assertFalse(true);
        } catch (InexistentUserException e) {
            Assert.assertTrue(true);
        }
        catch (Exception e) {
            logger.error(e.getMessage());
            Assert.assertFalse(true);
        }
    }
}
