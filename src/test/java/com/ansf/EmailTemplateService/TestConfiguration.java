package com.ansf.EmailTemplateService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

@org.springframework.boot.test.context.TestConfiguration
@PropertySource("classpath:application-test.properties")
public class TestConfiguration {

    @Value("${aws.s3.bucket}")
    private String _s3Bucket;

    @Value("${aws.s3.bucket.key}")
    private String _s3BucketKey;

    @Value("${aws.service.retry.count}")
    private Long _s3RetryCount;

    @Value("${aws.s3.region}")
    private String _s3Region;

    @Value("${security.digest.algorithm}")
    private String _algorithm;

    @Bean
    public String s3Bucket() {
        return _s3Bucket;
    }

    @Bean
    public String s3BucketKey() {
        return _s3BucketKey;
    }

    @Bean
    public Long s3RetryCount() { return _s3RetryCount; }

    @Bean
    public String checksumAlgorithm() {
        return _algorithm;
    }

    @Bean
    public String s3Region() { return _s3Region; }
}
