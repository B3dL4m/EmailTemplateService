package com.ansf.EmailTemplateService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(
		basePackages = {
				"com.ansf.EmailTemplateService.configuration",
				"com.ansf.EmailTemplateService.model",
				"com.ansf.EmailTemplateService.model.repository",
				"com.ansf.EmailTemplateService.rest",
				"com.ansf.EmailTemplateService.security",
				"com.ansf.EmailTemplateService.service"
		}
)
@EnableAutoConfiguration
public class EmailTemplateServiceApplication {

	public static void main(String[] args) {

		SpringApplication.run(EmailTemplateServiceApplication.class, args);
	}
}
