package com.ansf.EmailTemplateService.exception;

public class InexistentUserException extends InexistentEntityException {

    private static final String msg = "User with id %d does not exist";

    public InexistentUserException(final Long userId) {
        super(String.format(msg, userId));
    }
}
