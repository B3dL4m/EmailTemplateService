package com.ansf.EmailTemplateService.exception;

public class InexistentEntityException extends Exception {

    public InexistentEntityException(String message) {
        super(message);
    }

    public InexistentEntityException(String message, Throwable throwable) { super(message, throwable); }
}
