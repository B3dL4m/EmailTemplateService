package com.ansf.EmailTemplateService.exception;

public class InexistentBucketException extends InexistentEntityException {

    private static final String msg = "S3 bucket with name %s does not exist";

    public InexistentBucketException(String bucketName) {
        super(String.format(msg, bucketName));
    }

    public InexistentBucketException(String bucketName, Throwable throwable) {
        super(String.format(msg, bucketName));
    }
}
