package com.ansf.EmailTemplateService.exception;

public class InexistentTemplateException extends InexistentEntityException {

    private static final String msg = "Template with name %s does not exist";

    public InexistentTemplateException(final String name) {
        super(String.format(msg, name));
    }
}
