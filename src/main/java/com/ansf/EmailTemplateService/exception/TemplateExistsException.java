package com.ansf.EmailTemplateService.exception;

public class TemplateExistsException extends Exception {

    private static final String msg = "Template with name %s already exists";

    public TemplateExistsException(final String name) {
        super(String.format(msg, name));
    }
}
