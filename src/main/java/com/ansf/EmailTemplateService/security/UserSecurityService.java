package com.ansf.EmailTemplateService.security;

import com.ansf.EmailTemplateService.model.User;
import com.ansf.EmailTemplateService.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@Profile("prod")
public class UserSecurityService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(username);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("User with email %s does not exist", username));
        }

        return new org.springframework.security.core.userdetails.User(
                user.getEmail(),
                user.getPassword(),
                getAuthorities(user)
        );
    }

    private Collection<GrantedAuthority> getAuthorities(User user) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(listToCommaSeparatedString(user.getRoles()));
        return authorities;
    }

    private String listToCommaSeparatedString(List<?> list) {
        StringBuilder sb = new StringBuilder();
        list.forEach(role -> sb.append(role.toString() + ","));
        return sb.toString();
    }
}
