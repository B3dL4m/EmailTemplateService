package com.ansf.EmailTemplateService.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "USERS")
@SequenceGenerator(name = "UserIdGenerator", sequenceName = "UserIdSeq")
public class User {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserIdGenerator")
    private Long id;

    @Column(name = "email")
    @Email(message = "error.email.email")
    @NotNull(message = "error.email.empty")
    private String email;

    @Column(name = "password")
    @NotNull(message = "error.password.empty")
    private String password;

    @Column(name = "enabled")
    @JsonIgnore
    private boolean isEnabled;

    @OneToMany(
            cascade = CascadeType.ALL,
            mappedBy = "user",
            orphanRemoval = true
    )
    @JsonIgnore
    private List<Template> templates = new ArrayList<>();

    @OneToMany(
            cascade = CascadeType.ALL,
            mappedBy = "user",
            orphanRemoval = true
    )
    @JsonIgnore
    private List<UserRole> roles = new ArrayList<>();

    public User() {

    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User(Long id, String email, String password) {
        this(email, password);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public List<Template> getTemplates() {
        return templates;
    }

    public void setTemplates(List<Template> templates) {
        this.templates = templates;
    }

    public void addTemplate(Template template) {
        if (!templates.contains(template)) {
            templates.add(template);
            template.setUser(this);
        }
    }

    public void removeTemplate(Template template) {
        if (templates.contains(template)) {
            templates.remove(template);
            template.setUser(null);
        }
    }

    public void addTemplates(Collection<? extends Template> templates) {
        for (Template template : templates) {
            this.templates.add(template);
            template.setUser(this);
        }
    }

    public void removeTemplates(Collection<? extends Template> templates) throws EntityNotFoundException {
        for (Template template : templates) {
            this.templates.remove(template);
            template.setUser(null);
        }
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public List<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(List<UserRole> roles) {
        this.roles = roles;
    }
}
