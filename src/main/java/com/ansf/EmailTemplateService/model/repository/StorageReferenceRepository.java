package com.ansf.EmailTemplateService.model.repository;

import com.ansf.EmailTemplateService.model.StorageReference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StorageReferenceRepository extends JpaRepository<StorageReference, String> {

    StorageReference findByStorageId(final String storageId);
}
