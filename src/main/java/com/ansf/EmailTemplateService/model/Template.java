package com.ansf.EmailTemplateService.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "TEMPLATES")
public class Template {

    @Embeddable
    public static class PkTemplate implements Serializable {

        @Column(name = "user_id")
        private Long id;

        @Column(name = "alias")
        private String alias;

        public PkTemplate() {
        }

        public PkTemplate(Long id, String alias) {
            this.alias = alias;
            this.id = id;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getAlias() {
            return alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }
    }

    @EmbeddedId
    private PkTemplate userId = new PkTemplate();

    @Column(name = "storageId")
    private String storageId;

    @ManyToOne(
            cascade = { CascadeType.MERGE, CascadeType.REFRESH }
    )
    @MapsId("id")
    @JsonIgnore
    private User user;

    @Transient
    private byte[] contents;

    public Template() {
    }

    public Template(String name, String storageId) {
        this.userId.setAlias(name);
        this.storageId = storageId;
    }

    public Template(String name, String storageId, User user) {
        this(name, storageId);
        this.user = user;
    }

    public String getStorageId() {
        return storageId;
    }

    public void setStorageId(String location) {
        this.storageId = location;
    }

    public Long getId() {
        return userId.getId();
    }

    public void setId(Long id) {
        this.userId.setId(id);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        if (this.user != user) {
            this.user = user;
            if (user != null) {
                this.userId.setId(user.getId());
                user.addTemplate(this);
            }
        }
    }

    public void clearUser() {
        if (user != null) {
            user.removeTemplate(this);
            this.user = null;
        }
    }

    public void setName(String name) {
        this.userId.setAlias(name);
    }

    public String getName() {
        return userId.getAlias();
    }

    public byte[] getContents() {
        return contents;
    }

    public void setContents(byte[] contents) {
        this.contents = contents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Template template = (Template) o;
        return Objects.equals(userId, template.userId) &&
                Objects.equals(storageId, template.storageId) &&
                Objects.equals(user, template.user) &&
                Arrays.equals(contents, template.contents);
    }

    @Override
    public int hashCode() {

        int result = Objects.hash(userId, storageId, user);
        result = 31 * result + Arrays.hashCode(contents);
        return result;
    }
}
