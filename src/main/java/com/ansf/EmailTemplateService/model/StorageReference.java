package com.ansf.EmailTemplateService.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "STORAGE_REFERENCES")
public class StorageReference {

    @Id
    @Column(name = "storage_id")
    private String storageId;

    @Column(name = "reference_count")
    private Long count;

    public StorageReference() {
    }

    public StorageReference(String storageId, Long count) {
        this.storageId = storageId;
        this.count = count;
    }

    public String getStorageId() {
        return storageId;
    }

    public void setStorageId(String storageId) {
        this.storageId = storageId;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
