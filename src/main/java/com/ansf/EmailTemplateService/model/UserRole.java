package com.ansf.EmailTemplateService.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "USER_ROLES")
public class UserRole {

    @Embeddable
    private static class PkUserRole implements Serializable {

        @Column(name = "name")
        private String roleName;

        @Column(name = "user_id")
        private Long userId;

        public PkUserRole() {
        }

        public String getRoleName() {
            return roleName;
        }

        public void setRoleName(String roleName) {
            this.roleName = roleName;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }
    }

    @EmbeddedId
    private PkUserRole userRole = new PkUserRole();

    @ManyToOne(
            cascade = { CascadeType.MERGE, CascadeType.REFRESH }
    )
    @MapsId("userId")
    @JsonIgnore
    private User user;

    public UserRole() {
    }

    public UserRole(final String name, final Long userId) {
        this.userRole.roleName = name;
        this.userRole.userId = userId;
    }

    public String getRoleName() {
        return userRole.getRoleName();
    }

    public void setRoleName(String roleName) {
        userRole.setRoleName(roleName);
    }

    public Long getUserId() {
        return userRole.getUserId();
    }

    public void setUserId(Long userId) {
        userRole.setUserId(userId);
    }
}
