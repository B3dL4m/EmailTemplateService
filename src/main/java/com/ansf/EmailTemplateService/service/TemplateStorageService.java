package com.ansf.EmailTemplateService.service;

import com.amazonaws.AmazonServiceException;
import com.ansf.EmailTemplateService.exception.InexistentEntityException;
import com.ansf.EmailTemplateService.exception.TemplateExistsException;
import com.ansf.EmailTemplateService.model.Template;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface TemplateStorageService {

     List<Template> getTemplates(final Long userId) throws InexistentEntityException;

    Template getTemplate(final Long userId, final String templateName) throws InexistentEntityException, AmazonServiceException, IOException;

    void addTemplate(final Long userId, final String templateName, final byte[] contents) throws InexistentEntityException, NoSuchAlgorithmException, TemplateExistsException, RuntimeException;

    void removeTemplate(final Long userId, final String templateName) throws InexistentEntityException;

    void removeAllTemplates(final Long userId) throws InexistentEntityException;
}
