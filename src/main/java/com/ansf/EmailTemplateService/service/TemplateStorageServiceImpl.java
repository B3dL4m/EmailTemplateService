package com.ansf.EmailTemplateService.service;

import com.ansf.EmailTemplateService.exception.InexistentTemplateException;
import com.ansf.EmailTemplateService.exception.InexistentUserException;
import com.ansf.EmailTemplateService.model.Template;
import com.ansf.EmailTemplateService.model.User;
import com.ansf.EmailTemplateService.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;

import java.util.List;

public abstract class TemplateStorageServiceImpl implements TemplateStorageService {

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected ChecksumCalculator checksumCalculator;

    protected User getUserById(final Long userId) throws InexistentUserException {
        User user = userRepository.findById(userId).orElse(null);
        if (user == null)
            throw new InexistentUserException(userId);
        return user;
    }

    @Nullable
    private Template doGetTemplateByName(final User user, final String templateName) {
        Template template = null;
        List<Template> templates = user.getTemplates();
        for (Template t : templates) {
            if (t.getName().equals(templateName)) {
                template = t;
                break;
            }
        }
        return template;
    }

    protected Template getTemplateByName(final Long userId,final String templateName)
            throws InexistentUserException, InexistentTemplateException {
        User user = getUserById(userId);
        Template template = doGetTemplateByName(user, templateName);
        if (template == null)
            throw new InexistentTemplateException(templateName);
        return template;
    }

    protected Template getTemplateByName(final User user, final String templateName)
            throws InexistentTemplateException {
        Template template = doGetTemplateByName(user, templateName);
        if (template == null)
            throw new InexistentTemplateException(templateName);
        return template;
    }
}
