package com.ansf.EmailTemplateService.service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import com.ansf.EmailTemplateService.exception.InexistentEntityException;
import com.ansf.EmailTemplateService.exception.InexistentTemplateException;
import com.ansf.EmailTemplateService.exception.TemplateExistsException;
import com.ansf.EmailTemplateService.model.StorageReference;
import com.ansf.EmailTemplateService.model.Template;
import com.ansf.EmailTemplateService.model.User;
import com.ansf.EmailTemplateService.model.repository.StorageReferenceRepository;
import com.ansf.EmailTemplateService.model.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Profile;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

@Service
@Profile("prod")
public class AWSTemplateStorageService extends TemplateStorageServiceImpl {

    private static final Logger logger = LoggerFactory.getLogger(AWSTemplateStorageService.class);
    private static final Locale locale = LocaleContextHolder.getLocale();

    private StorageReferenceRepository storageReferenceRepository;
    private AmazonS3 s3Client;
    private MessageSource applicationProperties;

    @Autowired
    public AWSTemplateStorageService(UserRepository userRepository,
                                     StorageReferenceRepository storageReferenceRepository,
                                     AmazonS3 s3Client,
                                     MessageSource applicationProperties,
                                     ChecksumCalculator checksumCalculator) {
        this.userRepository = userRepository;
        this.storageReferenceRepository = storageReferenceRepository;
        this.s3Client = s3Client;
        this.applicationProperties = applicationProperties;
        this.checksumCalculator = checksumCalculator;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public AmazonS3 getS3Client() {
        return s3Client;
    }

    public void setS3Client(AmazonS3 s3Client) {
        this.s3Client = s3Client;
    }

    public MessageSource getApplicationProperties() {
        return applicationProperties;
    }

    public void setApplicationProperties(MessageSource applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public ChecksumCalculator getChecksumCalculator() {
        return checksumCalculator;
    }

    public void setChecksumCalculator(ChecksumCalculator checksumCalculator) {
        this.checksumCalculator = checksumCalculator;
    }

    public StorageReferenceRepository getStorageReferenceRepository() {
        return storageReferenceRepository;
    }

    public void setStorageReferenceRepository(StorageReferenceRepository storageReferenceRepository) {
        this.storageReferenceRepository = storageReferenceRepository;
    }

    private void getTemplateContentsFromS3(Template template)
            throws AmazonServiceException, IOException {
        String s3Bucket = applicationProperties.getMessage("aws.s3.bucket", null, locale);
        S3ObjectInputStream objStream = s3Client.getObject(s3Bucket, template.getStorageId()).getObjectContent();
        template.setContents(IOUtils.toByteArray(objStream));
    }

    private String constructBucketKey(final String templateName) {
        String bucketKey = applicationProperties.getMessage("aws.s3.bucket.key", null, locale);
        return bucketKey + '/' +templateName;
    }

    private void saveToStorage(final String s3BucketName, final String s3Key, final byte[] contents)
            throws SdkClientException, AmazonClientException, RuntimeException {
        if (!s3Client.doesBucketExistV2(s3BucketName)) {
            String regionId = applicationProperties.getMessage("aws.s3.region", null, locale);
            CreateBucketRequest request = new CreateBucketRequest(s3BucketName, regionId);
            s3Client.createBucket(request);
        }

        boolean objectExists = s3Client.doesObjectExist(s3BucketName, s3Key);
        StorageReference storageReference = storageReferenceRepository.findByStorageId(s3Key);

        if (objectExists && (storageReference != null)) {
            storageReference.setCount(storageReference.getCount() + 1);
        } else if (!objectExists && (storageReference == null)) {
            s3Client.putObject(s3BucketName, s3Key, new ByteArrayInputStream(contents), null);
            storageReference = new StorageReference(s3Key, 1L);
        } else {
            throw new RuntimeException("Inconsistency between reference map and stored objects");
        }

        storageReferenceRepository.saveAndFlush(storageReference);
    }

    private void doRemoveTemplate(final Long userId, final String templateName, final boolean removeFromUserEntity) throws InexistentEntityException {
        User user = getUserById(userId);
        Template template = getTemplateByName(user, templateName);

        if (removeFromUserEntity) {
            user.removeTemplate(template);
            userRepository.saveAndFlush(user);
        }

        StorageReference storageReference = storageReferenceRepository.findByStorageId(template.getStorageId());
        if ((storageReference.getCount() - 1) == 0) {
            storageReferenceRepository.delete(storageReference);
            storageReferenceRepository.flush();
        } else {
            storageReference.setCount(storageReference.getCount() - 1);
            storageReferenceRepository.saveAndFlush(storageReference);
        }
    }

    public List<Template> getTemplates(final Long userId) throws InexistentEntityException {
        return getUserById(userId).getTemplates();
    }

    public Template getTemplate(final Long userId, final String templateName)
            throws InexistentEntityException, AmazonServiceException, IOException {
        Template template = getTemplateByName(userId, templateName);
        Long retryCount = Long.valueOf(applicationProperties.getMessage("aws.service.retry.count", null, locale));
        while (true) {
            try {
                getTemplateContentsFromS3(template);
                break;
            } catch (AmazonClientException e) {
                logger.debug(e.getMessage());
                if (!e.isRetryable() || retryCount == 0) {
                    throw e;
                }
                --retryCount;
            }
        }
        return template;
    }

    public void addTemplate(final Long userId, final String templateName, final byte[] contents)
            throws InexistentEntityException, NoSuchAlgorithmException, TemplateExistsException, RuntimeException {
        User user = getUserById(userId);
        Template template;
        try {
            /* Check if a template with the same name already exists and
             * if it doesn't then continue.
             */
            template = getTemplateByName(user, templateName);
            throw new TemplateExistsException(templateName);
        } catch (InexistentTemplateException e) {
            logger.info(String.format("Adding template with name %s to user %d", templateName, userId));
        }
        String digestedFileName = checksumCalculator.calculate(contents);
        String s3Key = constructBucketKey(digestedFileName);
        template = new Template(templateName, s3Key);
        /* perform a hash of the file's contents */
        user.addTemplate(template);
        userRepository.saveAndFlush(user);
        String s3BucketName = applicationProperties.getMessage("aws.s3.bucket", null, locale);
        saveToStorage(s3BucketName, s3Key, contents);
    }

    public void removeTemplate(final Long userId, final String templateName)
            throws InexistentEntityException {
        doRemoveTemplate(userId, templateName, true);
    }

    public void removeAllTemplates(final Long userId)
            throws InexistentEntityException {
        User user = getUserById(userId);
        Collection<Template> templates = user.getTemplates();
        for (Template t : templates) {
            doRemoveTemplate(userId, t.getName(), false);
        }
        templates.clear();
        userRepository.saveAndFlush(user);
    }
}
