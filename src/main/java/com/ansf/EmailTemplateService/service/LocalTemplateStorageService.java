package com.ansf.EmailTemplateService.service;

import com.amazonaws.AmazonServiceException;
import com.ansf.EmailTemplateService.exception.InexistentEntityException;
import com.ansf.EmailTemplateService.exception.InexistentTemplateException;
import com.ansf.EmailTemplateService.exception.TemplateExistsException;
import com.ansf.EmailTemplateService.model.Template;
import com.ansf.EmailTemplateService.model.User;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
@Profile("dev")
public class LocalTemplateStorageService extends TemplateStorageServiceImpl {

    private Logger logger = LoggerFactory.getLogger(LocalTemplateStorageService.class);

    @Override
    public List<Template> getTemplates(Long userId) throws InexistentEntityException {
        return getUserById(userId).getTemplates();
    }

    @Override
    public Template getTemplate(Long userId, String templateName) throws InexistentEntityException, AmazonServiceException, IOException {
        return null;
    }

    @Override
    public void addTemplate(Long userId, String templateName, byte[] contents) throws InexistentEntityException, NoSuchAlgorithmException, TemplateExistsException, RuntimeException {
        User user = getUserById(userId);
        Template template;
        try {
            /* Check if a template with the same name already exists and
             * if it doesn't then continue.
             */
            template = getTemplateByName(user, templateName);
            throw new TemplateExistsException(templateName);
        } catch (InexistentTemplateException e) {
            logger.info(String.format("Adding template with name %s to user %d", templateName, userId));
        }
        String digestedFileName = checksumCalculator.calculate(contents);
        Path templatesDir = Paths.get(".", "templates", digestedFileName)
                                 .toAbsolutePath();
        try {
            File file = templatesDir.toFile();
            if (file.createNewFile() && file.canWrite()) {
                FileUtils.writeByteArrayToFile(file, contents);
            } else {
                logger.error(String.format("Could not create file %s", file.getAbsolutePath()));
                throw new IOException(String.format("Could not create file %s", file.getAbsolutePath()));
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public void removeTemplate(Long userId, String templateName) throws InexistentEntityException {

    }

    @Override
    public void removeAllTemplates(Long userId) throws InexistentEntityException {

    }
}
