package com.ansf.EmailTemplateService.service;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ChecksumCalculator {

    private String algorithm;

    public ChecksumCalculator(String algorithm) {
        this.algorithm = algorithm;
    }

    public ChecksumCalculator() {

    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String calculate(final byte[] input) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
        messageDigest.update(input);
        return new HexBinaryAdapter().marshal(messageDigest.digest());
    }
}
