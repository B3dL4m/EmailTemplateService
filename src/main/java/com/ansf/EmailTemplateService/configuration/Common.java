package com.ansf.EmailTemplateService.configuration;

import com.ansf.EmailTemplateService.service.ChecksumCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.util.Locale;

@Configuration
public class Common {

    private static Logger logger = LoggerFactory.getLogger(Common.class);
    private static Locale locale = LocaleContextHolder.getLocale();

    @Autowired
    private ReloadableResourceBundleMessageSource applicationProperties;

    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageBundle = new ReloadableResourceBundleMessageSource();
        messageBundle.setBasename("classpath:messages/messages");
        messageBundle.setDefaultEncoding("UTF-8");
        return messageBundle;
    }

    @Bean
    public ReloadableResourceBundleMessageSource applicationProperties() {
        ReloadableResourceBundleMessageSource applicationProperties = new ReloadableResourceBundleMessageSource();
        applicationProperties.setBasename("classpath:application");
        applicationProperties.setDefaultEncoding("UTF-8");
        return applicationProperties;
    }

    @Bean
    public ChecksumCalculator checksumCalculator() {
        String checksumAlgorithm = applicationProperties.getMessage("security.digest.algorithm", null, locale);
        return new ChecksumCalculator(checksumAlgorithm);
    }
}
