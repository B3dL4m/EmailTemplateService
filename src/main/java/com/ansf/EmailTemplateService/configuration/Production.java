package com.ansf.EmailTemplateService.configuration;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalk;
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalkClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

@Configuration
@Profile("prod")
public class Production {

    @Autowired
    private ReloadableResourceBundleMessageSource applicationProperties;

    @Bean
    public AmazonS3 s3Client() {
        AmazonS3ClientBuilder builder = AmazonS3ClientBuilder.standard();
        builder.setCredentials(new ProfileCredentialsProvider());
        return builder.build();
    }

    @Bean
    public AWSElasticBeanstalk ebClient() {
        AWSElasticBeanstalkClientBuilder builder = AWSElasticBeanstalkClientBuilder.standard();
        builder.setCredentials(new ProfileCredentialsProvider());
        return builder.build();
    }

    @Bean
    public AmazonSimpleEmailService sesClient() {
        AmazonSimpleEmailServiceClientBuilder builder = AmazonSimpleEmailServiceClientBuilder.standard();
        String region = applicationProperties.getMessage("aws.ses.region", null, LocaleContextHolder.getLocale());
        return builder.withRegion(region).build();
    }
}
