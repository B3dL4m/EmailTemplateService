package com.ansf.EmailTemplateService.rest;

import com.ansf.EmailTemplateService.exception.InexistentEntityException;
import com.ansf.EmailTemplateService.exception.TemplateExistsException;
import com.ansf.EmailTemplateService.model.Template;
import com.ansf.EmailTemplateService.service.TemplateStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;

@RestController
@RequestMapping("/user/{user_id}/template")
public class TemplateController {

    private static Logger logger = LoggerFactory.getLogger(TemplateController.class);
    private TemplateStorageService storageService;

    @Autowired
    public TemplateController(TemplateStorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping
    public ResponseEntity<?> getTemplates(@PathVariable("user_id") final Long id) {
        ResponseEntity<?> response;
        try {
            Collection<Template> templates = storageService.getTemplates(id);
            response = new ResponseEntity<>(templates, HttpStatus.OK);
        } catch (Exception e) {
            response = new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @GetMapping(value = "/{template_name}",
            produces = { MediaType.TEXT_HTML_VALUE, MediaType.TEXT_PLAIN_VALUE })
    public ResponseEntity<?> getTemplateByName(@PathVariable("user_id") final Long userId,
                                               @PathVariable("template_name") final String name) {
        ResponseEntity<?> response;
        Template template;
        try {
            template = storageService.getTemplate(userId, name);
            response = new ResponseEntity<>(template.getContents(), HttpStatus.OK);
        } catch (InexistentEntityException e) {
            String msg = e.getMessage();
            logger.error(msg, e.getCause());
            response = new ResponseEntity<>(msg, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            String msg = e.getMessage();
            logger.error(msg, e.getCause());
            response = new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> addTemplateForUser(@PathVariable("user_id") Long userId,
                                                @RequestParam("alias") String alias,
                                                @RequestParam("file") MultipartFile template) {
        ResponseEntity<?> response;
        try {
            storageService.addTemplate(userId, alias, template.getBytes());
            response = new ResponseEntity<>("Template saved with alias " + alias, HttpStatus.OK);
        } catch (TemplateExistsException e) {
            logger.error(e.getMessage());
            response = new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        } catch (Exception e) {
            logger.error(e.getMessage());
            response = new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @DeleteMapping("/{template_name}")
    public ResponseEntity<?> removeTemplate(@PathVariable("user_id") Long userId,
                                            @PathVariable("template_name") String name) {
        ResponseEntity<?> response;
        try {
            storageService.removeTemplate(userId, name);
            response = new ResponseEntity<>("Template " + name + " deleted", HttpStatus.OK);
        } catch (InexistentEntityException e) {
            logger.error(e.getMessage());
            response = new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error(e.getMessage());
            response = new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @DeleteMapping
    public ResponseEntity<?> removeAllTemplates(@PathVariable("user_id") Long userId) {
        ResponseEntity<?> response;
        try {
            storageService.removeAllTemplates(userId);
            response = new ResponseEntity<>("All templates removed for user " + userId, HttpStatus.OK);
        } catch (InexistentEntityException e) {
            logger.error(e.getMessage());
            response = new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error(e.getMessage());
            response = new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }
}
