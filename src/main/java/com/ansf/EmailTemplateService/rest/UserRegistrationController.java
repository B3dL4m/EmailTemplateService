package com.ansf.EmailTemplateService.rest;

import com.ansf.EmailTemplateService.model.User;
import com.ansf.EmailTemplateService.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserRegistrationController {

    private UserRepository repository;

    @Autowired
    public UserRegistrationController(UserRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public ResponseEntity<?> listUsers() {
        List<User> users = repository.findAll();

        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(@PathVariable("id") final Long id) {
        Optional<User> user = repository.findById(id);

        if (!user.isPresent())
            return new ResponseEntity<>("User with id " + id + " not found.", HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(user.get(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> createUser(@Valid @RequestBody final User user) {
        if(repository.findByEmail(user.getEmail()) != null)
            return new ResponseEntity<>(
                    "Unable to create new user. A User with email " + user.getEmail() + " already exists.",
                    HttpStatus.CONFLICT);

        repository.saveAndFlush(user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateUser(@PathVariable("id") final Long id,
                                        @Valid @RequestBody final User user) {
        Optional<User> currentUser = repository.findById(id);

        if (!currentUser.isPresent())
            return new ResponseEntity<>(
                    "User with id " + id + " not found.",
                    HttpStatus.NOT_FOUND);

        User _currentUser = currentUser.get();
        _currentUser.setEmail(user.getEmail());
        _currentUser.setPassword(user.getPassword());
        repository.saveAndFlush(_currentUser);

        return new ResponseEntity<>(_currentUser, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteUserById(@PathVariable("id") final Long id) {
        Optional<User> user = repository.findById(id);

        if (!user.isPresent())
            return new ResponseEntity<>(
                    "User with id " + id + " not found.",
                    HttpStatus.NOT_FOUND);

        repository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
