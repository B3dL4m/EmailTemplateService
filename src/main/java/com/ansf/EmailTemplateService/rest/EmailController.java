package com.ansf.EmailTemplateService.rest;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.*;
import com.ansf.EmailTemplateService.model.Template;
import com.ansf.EmailTemplateService.model.User;
import com.ansf.EmailTemplateService.model.repository.UserRepository;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Profile;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@RestController
@Profile("prod")
@RequestMapping("user/{user_id}/email")
public class EmailController {

    private static Logger logger = LoggerFactory.getLogger(EmailController.class);
    private static Locale locale = LocaleContextHolder.getLocale();
    @Autowired private UserRepository userRepository;
    @Autowired private AmazonSimpleEmailService sesClient;
    @Autowired private AmazonS3 s3Client;
    @Autowired private MessageSource applicationProperties;

    private @Nullable User getUser(final Long id) {
        return userRepository.findById(id).orElse(null);
    }

    private @Nullable Template getTemplate(final User user, final String name) {
        List<Template> templates = user.getTemplates();
        for (Template t : templates) {
            if (t.getName().equals(name)) {
                return t;
            }
        }

        return null;
    }

    private ResponseEntity<?> doSendEmail(final String mapping,
                                          final Long userId,
                                          final List<Long> targets,
                                          final String subject,
                                          @Nullable final MultipartFile templateFile,
                                          @Nullable final String templateAlias) {

        StringBuilder responseMsg = new StringBuilder();
        byte[] templateData;

        User user = getUser(userId);
        if (user == null)
            return new ResponseEntity<>("User with id " + userId + " does not exist.", HttpStatus.NOT_FOUND);

        if (targets.size() == 0)
            return new ResponseEntity<>("There are no target recipients provided.", HttpStatus.BAD_REQUEST);

        List<String> to = new ArrayList<>();
        for (Long id : targets) {
            User u = getUser(id);
            if (u == null) {
                responseMsg.append("User with id " + id + " does not exist. Excluding from send list.\n");
                continue;
            }
            to.add(u.getEmail());
        }

        if (to.size() == 0)
            return new ResponseEntity<>("None of the provided targets are valid.", HttpStatus.BAD_REQUEST);
        else {
            to.forEach(email ->
                    sesClient.verifyEmailAddress(new VerifyEmailAddressRequest().withEmailAddress(email))
            );
        }

        try {
            if (mapping.equals("") && (templateFile != null))
                templateData = templateFile.getBytes();
            else if (mapping.equals("/template/{template_name}") && (templateAlias != null)) {
                Template template = getTemplate(user, templateAlias);
                if (template == null)
                    return new ResponseEntity<>("Template with alias " + templateAlias + " does not exist", HttpStatus.NOT_FOUND);

                final String bucketName = applicationProperties.getMessage("aws.s3.bucket", null, locale);
                final String key = template.getStorageId();
                GetObjectRequest request = new GetObjectRequest(bucketName, key);
                S3Object s3Object = s3Client.getObject(request);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                IOUtils.copy(s3Object.getObjectContent(), outputStream);
                templateData = outputStream.toByteArray();
            }
            else
                throw new RuntimeException();

            SendEmailRequest request = new SendEmailRequest()
                    .withDestination(new Destination().withToAddresses(to))
                    .withMessage(new Message()
                            .withBody(new Body()
                                    .withHtml(new Content()
                                            .withCharset("UTF-8")
                                            .withData(new String(templateData))))
                            .withSubject(new Content(subject)))
                    .withSource(user.getEmail());
            sesClient.sendEmail(request);
            to.stream().forEach((String usr) -> {
                String msg = "Email sent to " + usr;
                logger.info(msg);
                responseMsg.append(msg + "\n");
            });
        } catch (Exception e) {
            String msg = e.getMessage();
            logger.error(msg, e.getCause());
            return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(responseMsg.toString(), HttpStatus.OK);

    }

    @PostMapping
    public ResponseEntity<?> sendEmailToTargets(@PathVariable(name = "user_id") final Long userId,
                                                @RequestParam("target") final List<Long> targets,
                                                @RequestParam("subject") final String subject,
                                                @RequestParam("template") final MultipartFile template) {

        return doSendEmail("", userId, targets, subject, template, null);
    }

    @PostMapping("/template/{template_name}")
    public ResponseEntity<?> sendEmailToTargetsFromExistingTemplate(@PathVariable("user_id") final Long userId,
                                                                    @PathVariable("template_name") final String name,
                                                                    @RequestParam("subject") final String subject,
                                                                    @RequestParam("target") final List<Long> targets) {

        return doSendEmail("/template/{template_name}", userId, targets, subject, null, name);
    }
}
